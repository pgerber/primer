# OpenShift / Docker Primer

## Getting an existing image

Main source for Docker images is https://hub.docker.com

Here few examples using [Python's official images](https://hub.docker.com/_/python/):

```
docker pull python:3.5
```

`python` is the image and `3.5` is the tag.

If you omit the tag, the special tag `latest` is fetched:

```
docker pull python
```

## Running an image

The basic command to run an image is `docker run`:

```
$ docker run -it python
Python 3.7.1 (default, Nov 16 2018, 22:26:09) 
[GCC 6.3.0 20170516] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 
```

`-it` creates a virtual terminal and is needed for anything interactive.

Every images defines a default command that executed, in the `python` image
it's `python` that started by default. Of course you can specify an
arbitrary command to execute:

```
$ docker run -it python bash
root@858f8f366ab6:/#
```

### Building you're own image

### Volumes

It's possible to make file and directories available in Docker containers.

First we checkout the `tocco-docs` repository:

```
git clone https://github.com/tocco/tocco-docs.git
cd tocco-docs
```

Now, if we want to build the documentation, we'll need Python, Sphinx and some other
dependencies. Instead of installing them locally, which can be quite a mess, let's
use the `python` docker image.

```
$ docker run -v "$PWD:/repo" -w /repo python bash
root@f42bbc7a055f:/repo# pip3 install -r requirements.txt
    <OUTPUT REMOVED>
root@f42bbc7a055f:/repo# pip3 install sphinx_rtd_theme
    <OUTPUT REMOVED>
root@f42bbc7a055f:/repo# make html
    <OUTPUT REMOVED>
root@f42bbc7a055f:/repo# exit   # leave container
```

`-v` is what's telling docker that you want to make a volume (directory or file) available in the
container. Syntax is SOURCE:TARGET where SOURCE is the path to the directory/file you want to make
available and TARGET the location withing the container where you'd like to make the image available.

Or, you could do the same non-interactively:

```
docker run -v "$PWD:/repo" -w /repo python bash -c 'pip3 install -r requirements.txt && pip3 install sphinx_rtd_theme && make html'
```

## OpenShift Sample Project

### Create a New Project

Create a new project [in the control interface](https://control.vshn.net/openshift/projects/appuio%20public).

Switch to the project. Assuming the name is `toco-demo`, do this:
```
oc project toco-demo
```

### Postgres

```
oc new-app \
    -e POSTGRESQL_USER=user \
    -e POSTGRESQL_PASSWORD=QrNUQFypYVb4UmJcXdmhTvJXkc6xS \
    -e POSTGRESQL_DATABASE=demo \
    postgresql:11
```

See also https://docs.openshift.com/online/using_images/db_images/postgresql.html


