#!/usr/bin/python3
import psycopg2

def main():
    conn = psycopg2.connect(database='demo', user='postgres', password='abc', host='localhost')

    with conn.cursor() as cur:
        cur.executemany('''
           CREATE TABLE data (
               id    primary key bigint,
               value text not null
           )
        ''', [])

if __name__ == '__main__':
    main()

