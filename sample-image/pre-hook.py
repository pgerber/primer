#!/usr/bin/python3
import psycopg2
import os

def main(host, db, user, passwd):
    conn = psycopg2.connect(database=db, user=user, password=passwd, host=host)

    with conn.cursor() as cur:
        cur.execute('''
            create table if not exists schema_version ( version bigint not null, ts timestamp default now() );
        ''')

        cur.execute('''SELECT coalesce(max(version), 0) from schema_version''')
        version = cur.fetchone()[0]

        if version <= 0:
            cur.execute('''
               CREATE TABLE data (
                   id    bigint not null primary key,
                   value text not null
               );
               INSERT INTO schema_version (version) VALUES(1);
            ''')
    conn.commit()

if __name__ == '__main__':
    db = os.environ.get('PG_DB', 'demo')
    host = os.environ.get('PG_HOST', 'localhost')
    user = os.environ.get('PG_USER', 'postgres')
    passwd = os.environ['PG_PASS']
    main(host, db, user, passwd)
